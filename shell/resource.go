package shell

import (
	"io/ioutil"
	"fmt"
	"math/rand"
	//"log"
	"time"
	"gitlab.sec.ibm.com/devops_go/terraform_plugin_shell/lib/cmd-runner"
	"github.com/hashicorp/terraform/helper/schema"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func resource() *schema.Resource {
	return &schema.Resource{
		Create: resourceCreate,
		Read:   resourceRead,
		Delete: resourceDelete,

		Schema: map[string]*schema.Schema{
			"cmd": &schema.Schema{
				Type:     schema.TypeString,
				Optional: false,
				Required: true,
				ForceNew: true,
			},
			"args": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				ForceNew: true,
				Elem:     &schema.Schema{Type: schema.TypeString},
			},
			"output": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Computed: true,
			},
		},
	}
}

func resourceCreate(d *schema.ResourceData, meta interface{}) error {
	d.SetId(fmt.Sprintf("%d", rand.Int()))

	cmd := d.Get("cmd").(string)

	var stringArgs []string

	if v, ok := d.GetOk("args"); ok {
		m := v.([]interface{})

		for _, e := range m {
			if e != nil {
				stringArgs = append(stringArgs,e.(string))
			}
		}
	}

	r := CMDRunner.Runner {}
	out,err := r.Run(cmd,stringArgs)
	if err != nil {
		return err
	}

	ioutil.WriteFile("/tmp/out.log", []byte(fmt.Sprintf("%+v\n", cmd)), 0644)
	ioutil.WriteFile("/tmp/out.log", []byte(fmt.Sprintf("%+v\n", stringArgs)), 0644)
	d.Set("output",out)
	return resourceRead(d,meta)
}

func resourceRead(d *schema.ResourceData, meta interface{}) error {
	return nil
}

func resourceDelete(d *schema.ResourceData, meta interface{}) error {
	d.SetId("")
	return nil
}
