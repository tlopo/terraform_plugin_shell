DIR ||= File.dirname __FILE__

require 'logger'
require "#{DIR}/run-cmd-live-output"


class NexusUploader
  @@url_prefix = 'https://nexus.sec.ibm.com/content/repositories/devops-releases'
  def initialize(file,artifact,version,group='devops')
    @group = group
    @artifact = artifact
    @version = version 
    @file = file
    raise "File #{file} not found" unless File.exists? file

    @url = "#{@@url_prefix}/#{@group}/#{@artifact}/#{@version}/#{@artifact}-#{@version}.tgz"
    cmd = 'curl -Sf -H "Content-Type: application/gzip"'
    cmd = "#{cmd} -H 'Authorization: Basic YWRtaW46SWg4QXZzIQ=='"
    cmd = "#{cmd} -X POST --data-binary @#{@file}"
    cmd = "#{cmd} #{@url}"
    @cmd = cmd 

    @logger = Logger.new STDOUT
  end
  
  def get_curl_cmd
    @cmd
  end

  def upload
    success = RunCmdLiveOutput.new(@cmd).runWithRetry
    @logger.info "Upload successful url: #{@url}" if success
    @logger.warn "Upload failed url: #{@url}" unless success
    success
  end
end
