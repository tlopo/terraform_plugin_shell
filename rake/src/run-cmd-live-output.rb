require 'logger'
require 'open3'

class RunCmdLiveOutput
  @@logger = Logger.new(STDOUT)

  def initialize(cmd, useScript=false, i=$stdin, o=$stdout, e=$stderr)
    if useScript
      `uname | grep -qi darwin`
      @cmd = $?.success? ? "script -q /dev/null #{cmd}" : "script -q -c '#{cmd}' /dev/null"
    else
      @cmd = cmd
    end

    @i=i; @o=o; @e=$stderr
    @attempts=0; @retryInterval=3;
  end

  def run(cmd=@cmd)
    @@logger.debug "Running [#{cmd}]"
    #`stty raw -echo`
    Open3.popen3(cmd) do |i, o, e, t|

      def is_running(pid)
        Process.getpgid(pid);  true
        rescue Errno::ESRCH; false
      end

      tout = Thread.new {o.each_char {|char| @o.write char}}
      terr = Thread.new {e.each_char {|char| @e.write char}}
      tin = Thread.new {@i.each_char {|char| i.write char }}

      while is_running t.pid do sleep 0.3 end
      tin.terminate
      #`stty echo -raw > /dev/null 2>&1`
      t.value == 0 ? true : false
    end
  end

  def runWithRetry(attempts=@attempts,retryInterval=@retryInterval)
    count = 1
    loop do
      success = run(@cmd)
      if !success && count <= attempts
        @@logger.debug "Retrying to run command attempt #{count} of #{attempts}"
        count = count + 1
        sleep retryInterval
        redo  
      end

      return success if success
      @@logger.debug "Failed to run '#{@cmd}'"
      return false
    end
  end
end
