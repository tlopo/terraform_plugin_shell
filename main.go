package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.sec.ibm.com/devops_go/terraform_plugin_shell/shell"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: shell.Provider})
}
