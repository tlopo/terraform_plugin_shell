package CMDRunner

import (
	"os/exec"
)

type Runner struct { }

func (r Runner) Run (cmdName string, cmdArgs []string ) (string, error){
	var (
		cmdOut []byte
		err    error
	)
	if cmdOut, err = exec.Command(cmdName, cmdArgs...).Output(); err != nil {
		return "",err
	}
	return string(cmdOut),nil
}
